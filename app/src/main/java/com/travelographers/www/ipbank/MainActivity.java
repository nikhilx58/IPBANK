package com.travelographers.www.ipbank;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.travelographers.www.ipbank.fragments.Page;


public class MainActivity extends AppCompatActivity implements Page.PageListener {

    public static final String URL_TO_LOAD = "urlToLoad";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




    }

    @Override
    protected void onResume() {
        super.onResume();

        Fragment pageFragment = new Page();
        Bundle bundle = new Bundle();
        bundle.putString(URL_TO_LOAD, getResources().getString(R.string.loading_page));
        pageFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.container_main, pageFragment).commit();

    }


}