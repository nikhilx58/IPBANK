package com.travelographers.www.ipbank.fragments;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.travelographers.www.ipbank.MainActivity;
import com.travelographers.www.ipbank.R;
import com.travelographers.www.ipbank.classes.MyWebClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class Page extends Fragment {

    private WebView webView;
    private WebSettings webSettings;
    private MainActivity mainActivity;
    private MyWebClient myWebClient;
    private Bundle bundle;
    private String urlToLoad;

    public Page() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainActivity = (MainActivity) context;
        myWebClient = new MyWebClient();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bundle = getArguments();
        urlToLoad = bundle.getString(MainActivity.URL_TO_LOAD);

        View layout = inflater.inflate(R.layout.fragment_page, container, false);

        webView = (WebView) layout.findViewById(R.id.webView);

        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(myWebClient);


        return layout;
    }
    @Override
    public void onResume() {
        super.onResume();

        webView.loadUrl(urlToLoad);

        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public interface PageListener {

    }


}